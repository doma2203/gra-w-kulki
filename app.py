from mainwin import Ui_MainWindow
from PyQt5.QtWidgets import QMainWindow, QFileDialog
import sys
from PyQt5 import QtCore, QtGui, QtWidgets
import sqlite3
from functools import partial
from itertools import product,count
from random import randrange
import array

class GameData(object):
    def __init__(self):
        self.level = 2
        self.clicks = 0
        self.points = 0
        self.playername = 'Player'
        self.gameicons=  [[False for x in range(9)] for y in range(9)]
        self.game =  [[False for x in range(9)] for y in range(9)]



class App(Ui_MainWindow):
    def __init__(self):
        super(App, self).__init__()
        self.mainwindow = QMainWindow()
        self.mainwindow.setWindowTitle("Gra w kulki")
        self.ui = self.setupUi(self.mainwindow)
        self.game_data = GameData()
        self.new_game.triggered.connect(self.on_change_or_reset)
        self.rank.triggered.connect(self.show_rank)
        self.quit.triggered.connect(self.on_exit)
        self.toolbar.easy.clicked.connect(partial(self.on_change_or_reset,1))
        self.toolbar.medium.clicked.connect(partial(self.on_change_or_reset, 2))
        self.toolbar.difficult.clicked.connect(partial(self.on_change_or_reset, 3))
        self.random_ball_position(5)
        # self.random_ball_position(2)
        # self.on_new_move()
        self.mainwindow.show()




    def save_points_question(self):
        self.exit_msg = QtWidgets.QMessageBox()
        self.exit_msg.setWindowTitle("Zapisywanie wyniku")
        self.exit_msg.setText("Zapisać wynik do bazy?")
        self.exit_msg.setIcon(QtWidgets.QMessageBox.Warning)
        self.exit_msg.setDefaultButton(QtWidgets.QMessageBox.Yes)
        self.exit_msg.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        return self.exit_msg.exec_()

    def on_exit(self):
        if self.game_data.points>0 and (self.save_points_question() == QtWidgets.QMessageBox.Yes):
            self.save_points_to_database()
        sys.exit(0)

    def on_change_or_reset(self,param=2):
        self.game_data=GameData()
        self.game_data.level=param
        self.toolbar.points_number.display(self.game_data.points)
        print(param)

    def save_points_to_database(self):

        self.game_data.playername, ok = QtWidgets.QInputDialog.getText(None, "Zapis do bazy", "Podaj nick:",
                                                              QtWidgets.QLineEdit.Normal, self.game_data.playername)
        if ok and self.game_data.playername != '':
            conn=sqlite3.connect('rank.db')
            c=conn.cursor()
            query="insert into {0} values ('{1}',{2})".format(self.map_level(),self.game_data.playername,self.game_data.points)
            c.execute(query)
            conn.commit()
            conn.close()

    def choose_color(self):
        ret=8
        if self.game_data.level==1:
            ret=6
        elif self.game_data.level==2:
            ret=8
        elif self.game_data.level==3:
            ret=10
        return ret

    def is_collision_y(self,ox,oy):
        collision = False
        if ox[0]==ox[1]:
            for i in range(oy[0],oy[1]+1):
                print(ox[1],i)
                if self.game_data.game[ox[1]][i]:
                    collision=True
                    break
        return collision

    def is_collision_x(self, ox, oy):
        collision = False;
        if oy[0] == oy[1]:
            for i in range(ox[0], ox[1] + 1):
                print(i, oy[1])
                if self.game_data.game[i][oy[1]]:
                    collision = True
                    break
        return collision


    def single_ball_move(self,pos,dest):
        first_x, last_x = (pos[0], dest[0]) if dest[0] > pos[0] else (dest[0], pos[0])
        first_y, last_y = (pos[1], dest[1]) if dest[1] > pos[1] else (dest[1], pos[1])
        x=(first_x,last_x)
        y=(first_y,last_y)
        print(x,y)
        #or self.is_collision_y(x,y):
        if dest[1]>pos[1] and self.is_collision_x(x,y):
            print("Kolizja!")
            return
        else:
            self.game_data.game[dest[0]][dest[1]] = self.game_data.gameicons[pos[0]][pos[1]]
            balltype= 'balls/' +str(self.game_data.gameicons[pos[0]][pos[1]])+'.png'
            self.gridLayoutWidget.cells[pos].setIcon(QtGui.QIcon(QtGui.QPixmap('')))
            self.gridLayoutWidget.cells[dest].setIcon(QtGui.QIcon(QtGui.QPixmap(balltype)))
            self.gridLayoutWidget.cells[dest].setIconSize(QtCore.QSize(70, 70))


    def available_moves(self,pos):
        return list(product([pos[0]],self.print_except(pos[1])))+ list(product( self.print_except(pos[0]),[pos[1]]))

    def move_ball(self,pos):
        others=lambda x:list(self.print_except(pos[x]))
        if self.game_data.clicks<5:
            # print(pos) #tu jest pozycja klikniecia!
            list(map(lambda a:a.setEnabled(True),self.gridLayoutWidget.cells.values()))
            self.game_data.clicks+=1
            print(self.game_data.clicks)
            list(map(lambda a: a.setEnabled(True), self.gridLayoutWidget.cells.values()))
            # list(map(lambda a: a.setEnabled(True),map(lambda a:a, self.gridLayoutWidget.cells.values())))
            # print(self.available_moves(pos))
            list(map(lambda a: self.gridLayoutWidget.cells[a].clicked.connect(partial(self.single_ball_move,pos,a)),self.available_moves(pos)))
        else:
            return

    def print_except(self,number):
        for i in range(0,9):
            if i==number:
                continue
            yield i

    def random_ball_position(self, ballnr=3):
        for i in range(ballnr):
            if self.move_is_possible():
                id_number = randrange(1, self.choose_color())
                balltype = 'balls/' + str(id_number) + '.png'
                pos = (randrange(0, 9), randrange(0, 9))
                self.game_data.gameicons[pos[0]][pos[1]]=id_number #tymczasowo
                self.gridLayoutWidget.cells[pos].setDisabled(False)
                self.gridLayoutWidget.cells[pos].setIcon(QtGui.QIcon(QtGui.QPixmap(balltype)))
                self.gridLayoutWidget.cells[pos].setIconSize(QtCore.QSize(70,70))
                self.gridLayoutWidget.cells[pos].clicked.connect(partial(self.move_ball,pos))
                # print(list(self.print_except(pos[0])))

            else:
                self.is_game_over()

    def on_new_move(self,tries=3):
        while self.move_is_possible():
                for i in range(tries+10):
                    id_number=randrange(1, self.choose_color())
                    balltype = 'balls/' + str(id_number) + '.png'
                    pos=(randrange(0,9),randrange(0,9))
                    #rozrzucamy wylosowane kulki wedlug pozycji
                    # jesli klikniemy na wylosowana, a potem na jakies inne miejsce i jest trasa i miejsce wolne = przenies
                    # w przeciwnym wypadku = nic nie rob ( a moze komunikat)
                    self.game_data[pos[0]][pos[1]]=id_number
        else:
            self.is_game_over( )


        # wylosuj 3 polozenia spośród {3,5,7} kolorów sprawdzajac, czy czegos tam juz nie ma.
        # sprawdz czy i ile kulek tego samego koloru jest w rzedzie i kwadracie

    def show_rank(self):
        conn=sqlite3.connect('rank.db')
        c=conn.cursor()
        query='select * from {0} order by points desc'.format(self.map_level())
        c.execute(query)
        self.rank_msg=QtWidgets.QMessageBox()
        self.rank_msg.setWindowTitle("Ranking - "+self.map_level())
        self.rank_msg.setIcon(QtWidgets.QMessageBox.Information)
        self.rank_msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        a=c.fetchall()
        b='\n'.join(str(e)+"\t"+str(f) for e,f in a)
        self.rank_msg.setText(b)
        conn.commit()
        conn.close()
        self.rank_msg.exec_()


    def move_is_possible(self):
        return not all(map(lambda x:all(x),self.game_data.game))

    def map_level(self):
        lvl={1:'easy',2:'medium',3:'hard'};
        return lvl[self.game_data.level]


    def is_game_over(self):
        # if all(map(lambda x: all(x), self.game_data.game)):
        self.game_over_msg=QtWidgets.QMessageBox()
        self.game_over_msg.setWindowTitle("Koniec gry")
        self.game_over_msg.setIcon(QtWidgets.QMessageBox.Warning)
        self.game_over_msg.setText("Koniec gry. Twój wynik: {0}. Czy chcesz zagrać jeszcze raz?".format(self.game_data.points))
        self.game_over_msg.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        ret = self.game_over_msg.exec_()
        if ret == QtWidgets.QMessageBox.No:
            self.on_exit()
        else:
            self.save_points_question()




