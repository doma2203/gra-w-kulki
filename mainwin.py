# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets
import random


class Ball(QtWidgets.QGraphicsView):
    def __init__(self):
        super(Ball,self).__init__()
        self.color=None
        self.item=QtGui.QImage
        # self.


class Playground(QtWidgets.QGridLayout):
    def __init__(self,root):
        self.root = root
        super(Playground,self).__init__(self.root)
        self.cells=dict()
        self.setSpacing(1)
        self.setObjectName('playground')
        self.add_cells(9,9)
        # cells = QtWidgets.QPushButton(self.root)
        # # # cells.close()
        # cells.clicked.disconnect()
        # cells.setIcon(QtGui.QIcon(QtGui.QPixmap('balls/1.png')))



    def add_cells(self,rows,columns):
        for i in range(0,rows):
            for j in range(0,columns):
                alias=(i,j)
                self.cells[alias]=QtWidgets.QPushButton(self.root)
                self.cells[alias].setMinimumSize(80,80)
                self.cells[alias].setDisabled(True)
                # self.cells[alias].setFlat(True)
                # balltype='balls/'+str(random.randrange(1,10))+'.png'
                # self.cells[alias].setIcon(QtGui.QIcon(QtGui.QPixmap(balltype)))
                # self.cells[alias].setIconSize(QtCore.QSize(70,70))
                self.cells[alias].setStyleSheet('QPushButton {background-color: white;}')
                # self.cells[alias].setObjectName("c{0}{1}".format(alias[0],alias[1]))
                self.addWidget(self.cells[alias],i+1,j+1,1,1)


class Toolset(QtWidgets.QFormLayout):
    def __init__(self,root):
        self.root = root
        super(Toolset, self).__init__(self.root)
        self.add_widgets()

    def add_widgets(self):
        self.points=QtWidgets.QGroupBox("Punkty",self.root)
        self.points.setObjectName("Punkty")
        self.points.setMinimumSize(680,100)
        self.points_number=QtWidgets.QLCDNumber(self.points)
        self.points_number.setGeometry(5,24,668,70)
        self.points_number.segmentStyle()
        #------------------------------------------------
        self.level=QtWidgets.QGroupBox("Poziom",self.root)
        self.level.setObjectName("Poziom")
        self.level.setGeometry(0,110,680,120)
        self.current_level=QtWidgets.QLineEdit(self.level)
        self.current_level.setReadOnly(True)
        self.current_level.setGeometry(30,35,620,30)
        self.easy=QtWidgets.QPushButton("Łatwy",self.level)
        self.easy.setObjectName("easy")
        self.easy.setGeometry(30,80,200,30)
        self.medium = QtWidgets.QPushButton("Średni", self.level)
        self.medium.setObjectName("medium")
        self.medium.setGeometry(240, 80, 200, 30)
        self.difficult = QtWidgets.QPushButton("Trudny", self.level)
        self.difficult.setObjectName("difficult")
        self.difficult.setGeometry(450, 80, 200, 30)



class Ui_MainWindow(object):
   def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(720, 1000)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 700, 40))
        self.menuBar.setObjectName("menuBar")
        self.menuGame = QtWidgets.QMenu(self.menuBar)
        self.menuGame.setTitle("Gra")
        self.menuGame.setObjectName("menuGra")
        self.new_game=QtWidgets.QAction()
        self.new_game.setText("Nowa gra")
        self.new_game.setShortcut('Ctrl+A')
        self.rank=QtWidgets.QAction()
        self.rank.setText("Ranking...")
        self.rank.setShortcut('Ctrl+R')
        self.quit=QtWidgets.QAction()
        self.quit.setText("Zamknij")
        self.quit.setShortcut('Ctrl+Q')
        self.menuGame.addAction(self.new_game)
        self.menuGame.addAction(self.rank)
        self.menuGame.addSeparator()
        self.menuGame.addAction(self.quit)
        self.menuBar.addMenu(self.menuGame)
        MainWindow.setMenuBar(self.menuBar)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.centralWidget.setGeometry(QtCore.QRect(10,20,700,700))
        self.layout=QtWidgets.QVBoxLayout()
        self.layout.setGeometry(QtCore.QRect(10, 30, 700, 700))
        self.gridLayoutWidget = Playground(self.centralWidget)

        self.formLayoutWidget = QtWidgets.QWidget(MainWindow)
        self.formLayoutWidget.setGeometry(QtCore.QRect(20,730,700,400))
        self.toolbar = Toolset(self.formLayoutWidget)
        self.statusBar = QtWidgets.QStatusBar(MainWindow)
        self.statusBar.setObjectName("statusBar")
        MainWindow.setStatusBar(self.statusBar)

